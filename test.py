from clis.cliread import *
from clis.cliwrite import *

parser = commandParser(keyword=['run', 'debug', 'compile'], arg=['c', 'f', '-file', '-code'],     
            leader=['-'], content='\w+\.cc|"[\w\W]+"', argvalue=True, strict=False)
print(parser.parse('codech -f -t run --code abc abc.cc'))

writer = commandWriter()
print(writer.write(keyword='run', arg={'c': 'test', '-file': ''}, leader='-', content='code.cc'))