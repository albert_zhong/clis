import re

class CommandWriter():
    def __init__(self):
        pass

    def write(self, keyword='', arg={}, leader='-', content=''):
        """
        Content 
            example character      codech    run               -f                file       test.cc   
            corresponding content  [name]  [keyword]  [leader][arg](anywhere)  [argvalue]  [content]
        Argument instruction
            keyword    optional     string    the character which means the function of command(only one in a command)
            arg        optional      dict     the arguments(without leader, can be more than one); 
                                              dict >> {arg: argvalue, };
                                              to express full named arg with symbol '--', 
                                              the value should be like ['-a', '-a-b'](must use leader '-')
            leader     optional     string     the leader symbol of the args
            content    optional     string    the content
        Return 
            sring >> the command
        Example
            writer.write(keyword='run', arg={'c': 'test', '-file': ''},     
                         leader='-', content='code.cc')
            -> 'run -c test --file code.cc
        """
        result = ''
        if keyword:
            result += keyword + ' '
        for a in arg:
            if arg[a]:
                result += leader + a + ' ' + arg[a]+ ' '
            else:
                result += leader + a + ' '
        if content:
            result += content
        return result