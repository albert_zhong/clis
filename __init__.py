from .cliread import *
from .cliwrite import *
import sys

class CliProgram():
    """
    Inherit this class to build a CLI program
    """
    def __init__(self):
        """
        Override this method.
        """
        pass

    def defineParser(self, keyword=[], arg=[], leader=['-', '/'], content='[A-Za-z]+', argvalue=True, strict=False):
        """
        Inherit this method.
        Content 
            example character      codech    run               -f                file       test.cc   
            corresponding content  [name]  [keyword]  [leader][arg](anywhere)  [argvalue]  [content]
        Argument instruction
            keyword    optional      list     the characters which mean the function of command(only one in a command)
            arg        optional    list/dict  the arguments(without leader, can be more than one); 
                                              dict >> {keyword: [corresponding args], };
                                              to express full named arg with symbol '--', 
                                              the value should be like ['-a', '-a-b'](must use leader '-')
            leader     optional      list     the leader symbol of the args
            content    optional    string:re  the rule to match the main content(or it may be matched as arg value)
            argvalue   optional      bool     whether allow having arg values
            strict     optional      bool     whether use strict mode
        Example
            parseCommand(name='codech', keyword=['run', 'debug', 'compile'], arg=['c', 'f', '-file', '-code'],     
            leader=['-'], content='\w+\.cc|"[\w\W]+"', argvalue=True)
        """
        self.parser = CommandParser(keyword, arg, leader, content, argvalue, strict)
        return self.parser

    def parseInput(self):
        """
        Inherit this method.
        """
        info = self.parser.parse(input())
        return info

    def parseArgv(self):
        """
        Inherit this method.
        """
        info = self.parser.parse(sys.argv)
        return info

    def run(self):
        """
        Override this method. Use dict to run with arguments.
        Example >>
            Dict = {'kw1': {'f': func1, 'c': func2}, 'kw2': {'a': func2}}
        """
        pass